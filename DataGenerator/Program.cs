﻿using System;
using DataGenerator.DataSets;
using DataGenerator.DataSets.Partners;
using DataGenerator.Extensions;
using FluentMigrator.Runner;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;

namespace DataGenerator
{
    public class Program
    {
        private static IServiceCollection _services;

        public static int Main(string[] args)
        {
            _services = new ServiceCollection();

            var app = new CommandLineApplication
            {
                Name = "data",
                Description = "Data generator for the integration work of the \"Strategic Data Management and Analysis\" course "
            };
            app.HelpOption(inherited: true);

            app.Command("generate", generate =>
            {
                generate.OnExecute(() =>
                {
                    Console.WriteLine("Specify a subcommand");
                    generate.ShowHelp();
                });

                generate.Command("database", database =>
                {
                    database.Description = "Generate database data";
                    var density = database.Argument("density", "density of data");

                    database.OnExecute(() =>
                    {
                        _services
                            .AddFluentMigratorCore()
                            .ConfigureRunner(rb => rb
                                .AddSqlServer()
                                .WithGlobalConnectionString(Database.ConnectionString)
                                .ScanIn(typeof(Program).Assembly).For.Migrations())
                            .AddLogging(lb => lb.AddFluentMigratorConsole());

                        var serviceProvider = _services.BuildServiceProvider();

                        using (var scope = serviceProvider.CreateScope())
                        {
                            UpdateDatabase(scope.ServiceProvider);
                        }

                        Loja.Seed();

                        Loja.Gerar(Convert.ToInt32(density.Value));
                    });
                });

                generate.Command("spreadsheet", spreedsheed =>
                {
                    spreedsheed.Description = "Generate partner spreadsheets";
                    spreedsheed.OnExecute(() =>
                    {
                        Console.WriteLine("Specify a partner");
                        generate.ShowHelp();
                    });

                    spreedsheed.Command("b2w", b2w =>
                    {
                        b2w.Description = "Generate b2w spreadsheets";
                        var density = b2w.Argument("density", "Data density, the higher the more data will be generated per period");

                        b2w.OnExecute(() =>
                        {
                            B2W
                               .Gerar(Convert.ToInt32(density.Value))
                               .SalvarArquivo();
                        });
                    });

                    spreedsheed.Command("carrefour", carrefour =>
                    {
                        carrefour.Description = "Generate carrefour spreadsheets";
                        var density = carrefour.Argument("density", "Data density, the higher the more data will be generated per period ");

                        carrefour.OnExecute(() =>
                        {
                            Carrefour
                               .Gerar(Convert.ToInt32(density.Value))
                               .SalvarArquivo();
                        });
                    });

                    spreedsheed.Command("magazine-luiza", magazine =>
                    {
                        magazine.Description = "Generate magazine luiza spreadsheets";
                        var density = magazine.Argument("density", "Data density, the higher the more data will be generated per period ");

                        magazine.OnExecute(() =>
                        {
                            MagazineLuiza
                               .Gerar(Convert.ToInt32(density.Value))
                               .SalvarArquivo();
                        });
                    });

                    spreedsheed.Command("via-varejo", viaVarejo =>
                    {
                        viaVarejo.Description = "Generate via varejo spreadsheets";
                        var density = viaVarejo.Argument("density", "Data density, the higher the more data will be generated per period ");

                        viaVarejo.OnExecute(() =>
                        {
                            ViaVarejo
                               .Gerar(Convert.ToInt32(density.Value))
                               .SalvarArquivo();
                        });
                    });
                });
            });

            app.OnExecute(() =>
            {
                Console.WriteLine("Specify a subcommand");
                app.ShowHelp();
                return 1;
            });

            try
            {
                return app.Execute(args);
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid command, try again | Message: {0}", e.Message);
                return -1;
            }
        }

        private static void UpdateDatabase(IServiceProvider serviceProvider)
        {
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            runner.MigrateUp();
        }
    }
}