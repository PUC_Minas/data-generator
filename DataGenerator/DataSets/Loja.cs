﻿using System;
using System.Linq;
using Bogus;
using Bogus.Extensions.Brazil;
using DataGenerator.Extensions;
using Database = DataGenerator.Extensions.Database;

namespace DataGenerator.DataSets {
    public class Loja {
        public static void Seed( ) {
            var p = Database.Count( "produto" );
            if ( p > 0 )
                return;

            var itens = Produtos.CarregarProdutos( ).Where( x => x.Preco >= 1 );

            Database.Insert( "fornecedor", itens.Select( x => new { nome = x.Nome_Fornecedor } ).Distinct( ).ToArray( ) );

            foreach ( var item in itens ) {
                var fornecedor = Database.Get( "fornecedor", new { nome = item.Nome_Fornecedor } );
                Database.Insert( "produto", new { nome = item.Nome, descricao = item.Descricao, preco = item.Preco, data_cadastro = item.Data_Cadastro, fornecedor_id = fornecedor.fornecedor_id } );
                Console.WriteLine( "Inserido produto de nome: {0}", item.Nome );
            }

            var filiais = new[ ]
            {
               new { nome = "Filial 1" },
               new { nome = "Filial 2" },
               new { nome = "Filial 3" },
               new { nome = "Filial 4" },
               new { nome = "Filial 5" },
            };
            Database.Insert( "filial", filiais );

            var lojas = new[ ]
            {
                new {nome = "Loja", parceiro = false, data_cadastro = new DateTime(2000, 01, 01)},
                new {nome = "B2W", parceiro = true, data_cadastro = new DateTime(2017, 01, 01)},
                new {nome = "Carrefour", parceiro = true, data_cadastro = new DateTime(2017, 01, 01)},
                new {nome = "Magazine Luiza", parceiro = true, data_cadastro = new DateTime(2017, 01, 01)},
                new {nome = "Via Varejo", parceiro = true, data_cadastro = new DateTime(2017, 01, 01)}
            };
            Database.Insert( "loja", lojas );

            var meios_pagamentos = new[ ]
            {
                new {descricao = "Boleto"},
                new {descricao = "Cartão de crédito"}
            };
            Database.Insert( "meio_pagamento", meios_pagamentos );

            var transportadoras = new[ ]
            {
                new {nome = "Brasspress Transportes Urgentes"},
                new {nome = "TNT Mercúrio Cargas e Encomendas Expressas"},
                new {nome = "Rodonaves Transportes e Encomendas"},
                new {nome = "Patrus Transportes Urgentes"},
                new {nome = "Jamef Transportes"},
                new {nome = "Alfa Transportes"},
                new {nome = "Expresso São Miguel"},
                new {nome = "Transportes Translovat"}
            };
            Database.Insert( "transportadora", transportadoras );
        }

        private static void GenerateClients( int years ) {
            var faker = new Faker( "pt_BR" );
            var quantidade = faker.Random.Number( 1000 * 12 * years, 1100 * 12 * years );

            for ( int i = 0; i < quantidade; i++ ) {

                var person = new Bogus.Person( "pt_BR" );

                if ( person.DateOfBirth >= new DateTime( 2010, 01, 01 ) ) {
                    person.DateOfBirth = new DateTime( 2010, person.DateOfBirth.Month, person.DateOfBirth.Day );
                }

                var tempo = 2021 - person.DateOfBirth.Year;

                var dataCadastro = person.DateOfBirth.AddYears( faker.Random.Int( 5, tempo ) );
                if ( dataCadastro > new DateTime( 2021, 01, 01 ) ) {
                    dataCadastro = new DateTime( 2021, 01, 01 );
                }

                var cliente = new {
                    nome = person.FullName,
                    data_nascimento = person.DateOfBirth,
                    documento = person.Cpf( false ),
                    data_cadastro = faker.Date.Between( dataCadastro, new DateTime( 2021, 01, 01 ) )
                };
                var cliente_ = Database.Insert( "cliente", cliente );

                var addres = person.Address.Street.Split( " " );

                var logradouro = new {
                    descricao = string.Join( " ", addres.Skip( 1 ) )
                };
                var logradouro_ = Database.Find( "logradouro", logradouro );

                var cidade = new {
                    descricao = person.Address.City
                };
                var cidade_ = Database.Find( "cidade", cidade );

                var estado = new {
                    descricao = person.Address.State
                };
                var estado_ = Database.Find( "estado", estado );

                var pais = new {
                    descricao = "Brasil"
                };
                var pais_ = Database.Find( "pais", pais );

                var cliente_endereco = new {
                    cliente_id = cliente_.cliente_id,
                    numero = addres.FirstOrDefault( ),
                    complemento = person.Address.Suite,
                    logradouro_id = logradouro_.logradouro_id,
                    cidade_id = cidade_.cidade_id,
                    estado_id = estado_.estado_id,
                    pais_id = pais_.pais_id,
                    cep = Convert.ToInt32( new string( person.Address.ZipCode.Where( char.IsDigit ).ToArray( ) ) )
                };
                var cliente_endereco_ = Database.Insert( "cliente_endereco", cliente_endereco );
                Console.WriteLine( "Inserido cliente {0} de {1}, com nome {2}", i, quantidade, cliente.nome );
            }
        }

        public static void Gerar( int years ) {
            GenerateClients( years );

            var filiais = Database.GetAll( "filial" );
            var meiosPagamentos = Database.GetAll( "meio_pagamento" );
            var trasnportadoras = Database.GetAll( "transportadora" );
            var clientes = Database.GetAll( "cliente" );
            var produtos = Database.GetAll( "produto" );
            var data_min = new DateTime( 2018, 01, 01 );
            var data_max = new DateTime( 2021, 01, 01 );

            var faker = new Faker( "pt_BR" );
            var quantidade = faker.Random.Number( 40000 * 12 * years, 45000 * 12 * years );


            for ( var i = 0; i < quantidade; i++ ) {
                var cliente = faker.PickRandom( clientes );
                var endereco = Database.Get( "cliente_endereco", new { cliente_id = cliente.cliente_id } );

                var pedido = new {
                    pedido_id = faker.Random.Guid( ),
                    cliente_id = cliente.cliente_id,
                    cliente_endereco_id = endereco.cliente_endereco_id,
                    filial_id = faker.PickRandom( filiais ).filial_id,
                    loja_id = 1,
                    meio_pagamento_id = faker.PickRandom( meiosPagamentos ).meio_pagamento_id,
                    data_hora_pedido = faker.Date.Between( data_min, data_max )
                };
                var pedido_ = Database.Insert( "pedido", pedido );

                var totalPedido = 0M;
                var total = faker.Random.Number( 1, 5 );
                for ( int produto = 0; produto < total; produto++ ) {
                    try {
                        var item = faker.PickRandom( produtos );
                        var pedido_produto = new {
                            pedido_id = pedido.pedido_id,
                            produto_id = item.produto_id
                        };
                        totalPedido += ( decimal ) item.preco;
                        Database.Insert( "pedido_produto", pedido_produto, false );
                    } catch ( Exception ) { }
                }

                var pedido_transportadora = new {
                    transportadora_id = faker.PickRandom( trasnportadoras ).transportadora_id,
                    pedido_id = pedido.pedido_id,
                    preco_frete = faker.Random.Decimal( 0, totalPedido - ( totalPedido * 0.3M ) ),
                    data_coleta = faker.Date.Between( pedido.data_hora_pedido.Date, pedido.data_hora_pedido.Date.AddDays( faker.Random.Number( 0, 2 ) ) ),
                    data_entrega = faker.Date.Between( pedido.data_hora_pedido.Date.AddDays( 2 ), pedido.data_hora_pedido.Date.AddDays( faker.Random.Number( 2, 7 ) ) )
                };
                Database.Insert( "pedido_transportadora", pedido_transportadora, false );
                Console.WriteLine( "Inserido pedido {0} de {1}, com identificador: {2}", i, quantidade, pedido.pedido_id );
            }
        }
    }
}