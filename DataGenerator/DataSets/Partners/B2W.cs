﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using DataGenerator.Extensions;

namespace DataGenerator.DataSets.Partners
{
    public class B2W : ICloneable
    {
        public string Marca { get; set; }
        public string NomeFantasia { get; set; }
        public DateTime DataPedido { get; set; }
        public DateTime DataPagamento { get; set; }
        public DateTime? DataEstorno { get; set; }
        public DateTime DataLiberacao { get; set; }
        public DateTime DataPrevistaPgto { get; set; }
        public string Lancamento { get; set; }
        public string RefPedido { get; set; }
        public Guid Entrega { get; set; }
        public string Tipo { get; set; }
        public string Status { get; set; }
        public decimal Valor { get; set; }
        public string Parcela { get; set; }
        public string MeioPgto { get; set; }
        public string ModelFinanceiro { get; set; }

        public object Clone()
        {
            var b2w = (B2W)MemberwiseClone();
            return b2w;
        }

        public B2W Clone(string tipo, string lancamento, decimal valor)
        {
            var b2w = (B2W)Clone();
            b2w.Tipo = tipo;
            b2w.Lancamento = lancamento;
            b2w.Valor = valor;
            return b2w;
        }

        public void Atulizar(string tipo, string lancamento, decimal valor)
        {
            Tipo = tipo;
            Lancamento = lancamento;
            Valor = valor;
        }

        public static Dictionary<string, List<B2W>> Gerar(int anos)
        {
            var marca = new Dictionary<string, string>
            {
                {"SHOP", "01" },
                {"ACOM", "02" },
                {"SUBA", "03" }
            };

            var status = new[] { "Unlocked" };

            var meiosPgtos = new[] { "a vista" };

            var modeloFinanceiro = new[] { "Quinzenal" };

            var data_min = new DateTime(2018, 01, 01);
            var data_max = new DateTime(2021, 01, 01);
            var pedidos = new Faker<B2W>("pt_BR")
                .RuleFor(x => x.Marca, f => f.PickRandom(marca.Keys.ToList()))
                .RuleFor(x => x.NomeFantasia, f => "Loja")
                .RuleFor(x => x.DataPedido, f => f.Date.Between(data_min, data_max))
                .RuleFor(x => x.DataPagamento, (f, x) => f.Date.Between(x.DataPedido, x.DataPedido.AddDays(3)))
                .RuleFor(x => x.DataEstorno, f => null)
                .RuleFor(x => x.DataLiberacao, (f, x) => f.Date.Between(x.DataPedido, x.DataPedido.AddDays(3)))
                .RuleFor(x => x.DataPrevistaPgto, (f, x) => f.Date.Between(x.DataPedido, x.DataPedido.AddDays(5)))
                .RuleFor(x => x.Lancamento, f => "Venda Marketplace")
                .RuleFor(x => x.Tipo, f => "Venda")
                .RuleFor(x => x.Status, f => f.PickRandom(status))
                .RuleFor(x => x.Valor, f => f.Random.Decimal(50, 12000, 2))
                .RuleFor(x => x.Entrega, f => f.Random.Guid())
                .RuleFor(x => x.RefPedido, (f, x) => marca[x.Marca] + "-" + x.Entrega.ToString())
                .RuleFor(x => x.Parcela, f => string.Empty)
                .RuleFor(x => x.MeioPgto, f => f.PickRandom(meiosPgtos))
                .RuleFor(x => x.ModelFinanceiro, f => f.PickRandom(modeloFinanceiro));

            var faker = new Faker("pt_BR");

            var vendas = pedidos.Generate(faker.Random.Int(30000 * (12 * anos), 32000 * (12 * anos)));
            var comissao = vendas.Select(x =>
            {
                var b2w = (B2W)x.Clone();
                b2w.Atulizar("Comissao", "Comissão cobrada sobre a venda", Math.Round(((b2w.Valor * 0.1M) * -1), 2));
                return b2w;
            });
            var frete = vendas.Select(x => x.Clone("Frete_B2W_Entrega", "Frete B2W Entrega", Math.Round(faker.Random.Decimal(-300, 0), 2)));

            var values = vendas
                .Concat(comissao)
                .Concat(frete)
                .OrderBy(x => x.DataPedido)
                .ThenBy(x => x.Entrega)
                .ToList();

            var result = values
                .GroupBy(x => new { x.DataPedido.Month, x.DataPedido.Year })
                .Select(x => new KeyValuePair<string, List<B2W>>($"{x.Key.Month}-{x.Key.Year}", x.ToList()))
                .ToDictionary(x => x.Key, y => y.Value);

            return result;
        }
    }
}