﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using DataGenerator.Extensions;

namespace DataGenerator.DataSets.Partners
{
    public class Carrefour : ICloneable
    {
        public string TipoTransacao { get; set; }
        public string AjusteManual { get; set; }
        public int IdLoja { get; set; }
        public string Loja { get; set; }
        public string Periodo { get; set; }
        public string StatusCiclo { get; set; }
        public Guid IdPedido { get; set; }
        public decimal TotalPedido { get; set; }
        public string StatusPedido { get; set; }
        public DateTime RealizadoEm { get; set; }
        public DateTime PagoEm { get; set; }
        public DateTime EntregueEm { get; set; }
        public string DevolvidoCanceladoEm { get; set; }
        public string MotivoDevolucaoCancelamento { get; set; }
        public string DataProcessamentoIncidente { get; set; }
        public string MotivoIncidente { get; set; }
        public string Produto { get; set; }
        public decimal ValorUnitario { get; set; }
        public decimal ValorUnitarioPromocional { get; set; }
        public decimal FreteUnitario { get; set; }
        public int Quantidade { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPromocional { get; set; }
        public string GradeAssociada { get; set; }
        public decimal ComissaoPorcentagem { get; set; }
        public decimal Comissao { get; set; }
        public decimal Saldo { get; set; }

        public object Clone()
        {
            var b2w = (Carrefour)MemberwiseClone();
            return b2w;
        }

        public static Dictionary<string, List<Carrefour>> Gerar(int anos)
        {
            var produtos = Produtos.CarregarProdutos();

            var data_min = new DateTime(2018, 01, 01);
            var data_max = new DateTime(2021, 01, 01);

            var pedidos = new Faker<Carrefour>("pt_BR")
                .RuleFor(x => x.TipoTransacao, f => "Crédito")
                .RuleFor(x => x.AjusteManual, f => "")
                .RuleFor(x => x.IdLoja, f => 9999)
                .RuleFor(x => x.Loja, f => "Loja")
                .RuleFor(x => x.RealizadoEm, f => f.Date.Between(data_min, data_max))
                .RuleFor(x => x.Periodo, (f, x) => $"{new DateTime(x.RealizadoEm.Year, x.RealizadoEm.Month, x.RealizadoEm.Day).ToString("dd/MM/yyyy")} - {new DateTime(x.RealizadoEm.Year, x.RealizadoEm.Month, DateTime.DaysInMonth(x.RealizadoEm.Year, x.RealizadoEm.Month)).ToString("dd/MM/yyyy")}")
                .RuleFor(x => x.StatusCiclo, f => "Fechado")
                .RuleFor(x => x.IdPedido, f => f.Random.Guid())
                .RuleFor(x => x.Produto, f => f.PickRandom(produtos).Descricao)
                .RuleFor(x => x.StatusPedido, f => "Entregue")
                .RuleFor(x => x.PagoEm, (f, x) => x.RealizadoEm.AddDays(f.Random.Int(0, 10)))
                .RuleFor(x => x.EntregueEm, (f, x) => x.PagoEm.AddDays(f.Random.Int(0, 2)))
                .RuleFor(x => x.DevolvidoCanceladoEm, f => "")
                .RuleFor(x => x.MotivoDevolucaoCancelamento, f => "")
                .RuleFor(x => x.DataProcessamentoIncidente, f => "")
                .RuleFor(x => x.MotivoIncidente, f => "")
                .RuleFor(x => x.GradeAssociada, f => f.Random.Double(0, 1) >= 0.95 ? "Promotional" : "Seller")
                .RuleFor(x => x.ValorUnitario, (f, x) => Math.Round(produtos.First(y => y.Descricao == x.Produto).Preco, 2))
                .RuleFor(x => x.ValorUnitarioPromocional, (f, x) => x.GradeAssociada == "Promotional" ? Math.Round(x.ValorUnitario - (x.ValorUnitario * f.Random.Decimal(0.1M, 0.5M)), 2) : x.ValorUnitario)
                .RuleFor(x => x.FreteUnitario, (f, x) => f.Random.Decimal(0M, x.ValorUnitario * 0.3M, 2))
                .RuleFor(x => x.Quantidade, (f, x) => x.ValorUnitario < 30 ? f.Random.Int(1, 10) : 1)
                .RuleFor(x => x.Total, (f, x) => Math.Round((x.ValorUnitario * x.Quantidade) + (x.FreteUnitario * x.Quantidade), 2))
                .RuleFor(x => x.TotalPromocional, (f, x) => Math.Round((x.ValorUnitarioPromocional * x.Quantidade) + (x.FreteUnitario * x.Quantidade), 2))
                .RuleFor(x => x.TotalPedido, (f, x) => x.TotalPromocional)
                .RuleFor(x => x.ComissaoPorcentagem, (f, x) => x.GradeAssociada == "Promotional" ? 6 : 10)
                .RuleFor(x => x.Comissao, (f, x) => Math.Round(x.TotalPedido * (x.ComissaoPorcentagem / 100), 2))
                .RuleFor(x => x.Saldo, (f, x) => Math.Round(x.TotalPromocional - x.Comissao, 2))
                ;

            var faker = new Faker("pt_BR");

            var values = pedidos
                .Generate(faker.Random.Int(1200 * (12 * anos), 1500 * (12 * anos)))
                .OrderBy(x => x.RealizadoEm)
                .ThenBy(x => x.PagoEm)
                .ThenBy(x => x.EntregueEm)
                .ToList();

            var result = values
                .GroupBy(x => new { x.RealizadoEm.Month, x.RealizadoEm.Year })
                .Select(x => new KeyValuePair<string, List<Carrefour>>($"{x.Key.Month}-{x.Key.Year}", x.ToList()))
                .ToDictionary(x => x.Key, y => y.Value);

            return result;
        }
    }
}