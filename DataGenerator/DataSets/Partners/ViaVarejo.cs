﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using DataGenerator.Extensions;

namespace DataGenerator.DataSets.Partners
{
    public class ViaVarejo : ICloneable
    {
        public string TipoTransacao { get; set; }
        public Guid NumeroPedido { get; set; }
        public DateTime DataEnvio { get; set; }
        public DateTime DataNotificacao { get; set; }
        public string DescricaoProduto { get; set; }
        public string TipoPagamento { get; set; }
        public decimal TotalPedido { get; set; }
        public string Parcela { get; set; }
        public decimal ValorCiclo { get; set; }
        public decimal ValorComissao { get; set; }
        public decimal ValorRepasse { get; set; }
        public decimal ValorItem { get; set; }
        public decimal ValorFrete { get; set; }
        public string TipoFrete { get; set; }
        public string Origem { get; set; }

        public object Clone()
        {
            var b2w = (ViaVarejo)MemberwiseClone();
            return b2w;
        }

        public static Dictionary<string, List<ViaVarejo>> Gerar(int anos)
        {
            var origem = new[] { "2 - Extra", "3 - Casas Bahia", "4 - Ponto Frio" };

            var produtos = Produtos.CarregarProdutos();

            var tipoPagamentos = new Dictionary<string, string>()
            {
                {"Cartão de crédito", "1/1"},
                {"Boleto a vista", "À VISTA" }
            };

            var data_min = new DateTime(2018, 01, 01);
            var data_max = new DateTime(2021, 01, 01);

            var pedidos = new Faker<ViaVarejo>("pt_BR")
                .RuleFor(x => x.TipoTransacao, f => "Venda")
                .RuleFor(x => x.NumeroPedido, f => f.Random.Guid())
                .RuleFor(x => x.DataEnvio, f => f.Date.Between(data_min, data_max))
                .RuleFor(x => x.DataNotificacao, (f, x) => f.Date.Between(x.DataEnvio, x.DataEnvio.AddDays(7)))
                .RuleFor(x => x.DescricaoProduto, f => f.PickRandom(produtos).Descricao.Trim())
                .RuleFor(x => x.TipoPagamento, f => f.PickRandom(tipoPagamentos.Keys.ToList()))
                .RuleFor(x => x.Parcela, (f, x) => tipoPagamentos[x.TipoPagamento])
                .RuleFor(x => x.ValorItem, (f, x) => Math.Round(produtos.First(y => y.Descricao.Trim() == x.DescricaoProduto).Preco, 2))
                .RuleFor(x => x.ValorComissao, (f, x) => Math.Round(x.ValorItem * 0.1M, 2))
                .RuleFor(x => x.ValorFrete, (f, x) => Math.Round(f.Random.Decimal(0, x.ValorItem * 0.3M), 2))
                .RuleFor(x => x.ValorCiclo, (f, x) => Math.Round(x.ValorItem + x.ValorFrete, 2))
                .RuleFor(x => x.ValorRepasse, (f, x) => Math.Round(x.ValorCiclo - x.ValorComissao, 2))
                .RuleFor(x => x.TotalPedido, (f, x) => Math.Round(x.ValorCiclo, 2))
                .RuleFor(x => x.TipoFrete, f => "NORMAL")
                .RuleFor(x => x.Origem, f => f.PickRandom(origem));

            var faker = new Faker("pt_BR");

            var values = pedidos
                .Generate(faker.Random.Int(3000 * (12 * anos), 3200 * (12 * anos)))
                .OrderBy(x => x.DataEnvio)
                .ThenBy(x => x.DataNotificacao)
                .ToList();

            var result = values
                .GroupBy(x => new { x.DataEnvio.Month, x.DataEnvio.Year })
                .Select(x => new KeyValuePair<string, List<ViaVarejo>>($"{x.Key.Month}-{x.Key.Year}", x.ToList()))
                .ToDictionary(x => x.Key, y => y.Value);

            return result;
        }
    }
}