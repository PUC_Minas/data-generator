﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using DataGenerator.Extensions;

namespace DataGenerator.DataSets.Partners
{
    public class MagazineLuiza : ICloneable
    {
        public DateTime DataTransacao { get; set; }
        public DateTime DataPedido { get; set; }
        public Guid IdPedidoMagazineLuiza { get; set; }
        public string IdPedidoSeller { get; set; }
        public string NumeroNotaFiscal { get; set; }
        public string NomeCliente { get; set; }
        public string MetodoPagamento { get; set; }
        public int ParcelaAtual { get; set; }
        public int TotalParcelas { get; set; }
        public decimal ValorLiquidoParcela { get; set; }
        public decimal ValorAntecipacao { get; set; }
        public int TaxaAntecipacao { get; set; }
        public decimal CommisaoMLParcela { get; set; }
        public decimal ValorBrutoParcela { get; set; }
        public decimal ValorBrutoPedido { get; set; }
        public decimal Comissao { get; set; }
        public decimal ValorComissao { get; set; }
        public decimal ValorBrutoSeller { get; set; }
        public string Origem { get; set; }
        public string Observacoces { get; set; }

        public object Clone()
        {
            var b2w = (MagazineLuiza)MemberwiseClone();
            return b2w;
        }

        public static Dictionary<string, List<MagazineLuiza>> Gerar(int anos)
        {
            var metodoPagamento = new[] { "Boleto", "Cartão de Crédito" };

            var data_min = new DateTime(2018, 01, 01);
            var data_max = new DateTime(2021, 01, 01);

            var pedidos = new Faker<MagazineLuiza>("pt_BR")
                .RuleFor(x => x.DataPedido, f => f.Date.Between(data_min, data_max))
                .RuleFor(x => x.DataTransacao, (f, x) => new DateTime(x.DataPedido.Year, x.DataPedido.Month, 5))
                .RuleFor(x => x.IdPedidoMagazineLuiza, f => f.Random.Guid())
                .RuleFor(x => x.IdPedidoSeller, (f, x) => $"LU-{x.IdPedidoMagazineLuiza}")
                .RuleFor(x => x.NumeroNotaFiscal, f => string.Empty)
                .RuleFor(x => x.NomeCliente, f => f.Person.FullName)
                .RuleFor(x => x.MetodoPagamento, f => f.PickRandom(metodoPagamento))
                .RuleFor(x => x.ParcelaAtual, f => 1)
                .RuleFor(x => x.TotalParcelas, f => 1)
                .RuleFor(x => x.ValorAntecipacao, f => 0)
                .RuleFor(x => x.TaxaAntecipacao, f => 0)
                .RuleFor(x => x.ValorBrutoPedido, f => f.Random.Decimal(5M, 6000M, 2))
                .RuleFor(x => x.ValorBrutoParcela, (f, x) => x.ValorBrutoPedido)
                .RuleFor(x => x.Comissao, f => f.Random.Decimal(6.49M, 9.6M, 2))
                .RuleFor(x => x.CommisaoMLParcela, (f, x) => Math.Round(x.ValorBrutoPedido * (x.Comissao / 100), 2))
                .RuleFor(x => x.ValorComissao, (f, x) => x.CommisaoMLParcela)
                .RuleFor(x => x.ValorBrutoSeller, (f, x) => x.ValorBrutoPedido - x.CommisaoMLParcela)
                .RuleFor(x => x.ValorLiquidoParcela, (f, x) => x.ValorBrutoSeller)
                .RuleFor(x => x.Origem, f => "Novo Magalu Pagamentos")
                .RuleFor(x => x.Observacoces, f => string.Empty)
                ;

            var faker = new Faker("pt_BR");

            var values = pedidos
                .Generate(faker.Random.Int(90000 * (12 * anos), 92000 * (12 * anos)))
                .OrderBy(x => x.DataPedido)
                .ToList();

            var result = values
                .GroupBy(x => new { x.DataPedido.Month, x.DataPedido.Year })
                .Select(x => new KeyValuePair<string, List<MagazineLuiza>>($"{x.Key.Month}-{x.Key.Year}", x.ToList()))
                .ToDictionary(x => x.Key, y => y.Value);

            return result;
        }
    }
}