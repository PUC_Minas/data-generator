﻿using FluentMigrator;

namespace DataGenerator.Migrations
{
    [Migration(00000000000001)]
    public class AddTables : Migration
    {
        public override void Up()
        {
            Create
                .Table("loja")
                .WithColumn("loja_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("nome").AsString().NotNullable()
                .WithColumn("parceiro").AsBoolean().NotNullable()
                .WithColumn("data_cadastro").AsDate().NotNullable()
                .WithColumn("data_fim").AsDate().Nullable()
                ;

            Create
                .Table("transportadora")
                .WithColumn("transportadora_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("nome").AsString().NotNullable();

            Create
                .Table("filial")
                .WithColumn("filial_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("nome").AsString().NotNullable();

            Create
                .Table("fornecedor")
                .WithColumn("fornecedor_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("nome").AsString().NotNullable();

            Create
                .Table("produto")
                .WithColumn("produto_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("nome").AsString().NotNullable()
                .WithColumn("descricao").AsString(500).Nullable()
                .WithColumn("preco").AsDecimal(15, 2)
                .WithColumn("fornecedor_id").AsInt64().NotNullable().ForeignKey("produto_fk_fornecedor", "fornecedor", "fornecedor_id")
                .WithColumn("data_cadastro").AsDateTime().NotNullable()
                ;

            Create
                .Table("cliente")
                .WithColumn("cliente_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("nome").AsString().NotNullable()
                .WithColumn("documento").AsString().NotNullable()
                .WithColumn("data_nascimento").AsDate().NotNullable()
                .WithColumn("data_cadastro").AsDateTime().NotNullable()
                ;

            Create
                .Table("logradouro")
                .WithColumn("logradouro_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("descricao").AsString().NotNullable();

            Create
                .Table("cidade")
                .WithColumn("cidade_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("descricao").AsString().NotNullable();

            Create
                .Table("estado")
                .WithColumn("estado_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("descricao").AsString().NotNullable();

            Create
                .Table("pais")
                .WithColumn("pais_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("descricao").AsString().NotNullable();

            Create
                .Table("cliente_endereco")
                .WithColumn("cliente_endereco_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("cliente_id").AsInt64().NotNullable().ForeignKey("cliente_endereco_fk_cliente", "cliente", "cliente_id")
                .WithColumn("numero").AsInt32().NotNullable()
                .WithColumn("cep").AsInt32().NotNullable()
                .WithColumn("complemento").AsString().Nullable()
                .WithColumn("logradouro_id").AsInt64().NotNullable().ForeignKey("cliente_endereco_fk_logradouro", "logradouro", "logradouro_id")
                .WithColumn("cidade_id").AsInt64().NotNullable().ForeignKey("cliente_endereco_fk_cidade", "cidade", "cidade_id")
                .WithColumn("estado_id").AsInt64().NotNullable().ForeignKey("cliente_endereco_fk_estado", "estado", "estado_id")
                .WithColumn("pais_id").AsInt64().NotNullable().ForeignKey("cliente_endereco_fk_pais", "pais", "pais_id")
                ;

            Create
                .Table("meio_pagamento")
                .WithColumn("meio_pagamento_id").AsInt64().PrimaryKey().Identity()
                .WithColumn("descricao").AsString();

            Create
                .Table("pedido")
                .WithColumn("pedido_id").AsGuid().PrimaryKey()
                .WithColumn("cliente_id").AsInt64().NotNullable().ForeignKey("pedido_fk_cliente", "cliente", "cliente_id")
                .WithColumn("cliente_endereco_id").AsInt64().NotNullable().ForeignKey("pedido_fk_cliente_endereco", "cliente_endereco", "cliente_endereco_id")
                .WithColumn("filial_id").AsInt64().NotNullable().ForeignKey("pedido_fk_filial", "filial", "filial_id")
                .WithColumn("loja_id").AsInt64().NotNullable().ForeignKey("pedido_fk_loja", "loja", "loja_id")
                .WithColumn("meio_pagamento_id").AsInt64().NotNullable().ForeignKey("pedido_fk_meio_pagamento", "meio_pagamento", "meio_pagamento_id")
                .WithColumn("data_hora_pedido").AsDateTime().NotNullable()
                ;

            Create
                .Table("pedido_produto")
                .WithColumn("pedido_id").AsGuid().PrimaryKey().ForeignKey("pedido_produto_fk_pedido", "pedido", "pedido_id")
                .WithColumn("produto_id").AsInt64().PrimaryKey().ForeignKey("pedido_produto_fk_produto", "produto", "produto_id")
                ;

            Create
                .Table("pedido_transportadora")
                .WithColumn("transportadora_id").AsInt64().PrimaryKey().ForeignKey("pedido_transportadora_fk_transportadora", "transportadora", "transportadora_id")
                .WithColumn("pedido_id").AsGuid().PrimaryKey().ForeignKey("pedido_transportadora_fk_pedido", "pedido", "pedido_id")
                .WithColumn("preco_frete").AsDecimal(15, 2).NotNullable()
                .WithColumn("data_coleta").AsDateTime().NotNullable()
                .WithColumn("data_entrega").AsDateTime().Nullable()
                ;
        }

        public override void Down()
        {
        }
    }
}