﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Syroot.Windows.IO;

namespace DataGenerator.Extensions
{
    public static class DictionaryExtensions
    {
        public static void SalvarArquivo<T>(this Dictionary<string, List<T>> data)
        {
            var options = new TypeConverterOptions { Formats = new[] { "dd/MM/yyyy" } };
            var configuration = new CsvConfiguration(new CultureInfo("pt-BR"))
            {
                Delimiter = ";"
            };

            var partner = typeof(T).Name;
            Console.WriteLine("Generating data for the partner: {0}", partner);

            foreach (var month in data)
            {
                var path = Path.Combine(new KnownFolder(KnownFolderType.Downloads).Path, "Planilhas", partner, month.Key + ".csv");
                var file = new FileInfo(path);
                if (!file.Directory.Exists) file.Directory.Create();

                using var writer = new StreamWriter(file.FullName, false);
                using var csv = new CsvWriter(writer, configuration);
                csv.Context.TypeConverterOptionsCache.AddOptions<DateTime>(options);
                csv.WriteRecords(month.Value);

                Console.WriteLine("\tWriting file to date: {0}", month.Key);
            }
        }
    }
}