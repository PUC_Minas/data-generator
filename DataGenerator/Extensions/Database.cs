﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;

namespace DataGenerator.Extensions
{
    public class Database
    {
        public static readonly string ConnectionString = @"Server=ORION\SQLEXPRESS;Database=vendas;User Id=sa;Password=!@12QWqw;";

        public static dynamic Insert(string table, object param, bool hasKey = true)
        {
            var fields = param.GetProperties();

            var sql = $"INSERT INTO {table} ({string.Join(", ", fields)}) Values ({string.Join(", ", fields.Select(x => "@" + x))});";

            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            connection.Execute(sql, param);
            connection.Close();

            if (hasKey)
                return Last(table);
            return null;
        }

        public static dynamic Get(string table, object param)
        {
            var fields = param.GetProperties();

            string sql = $"SELECT * FROM {table} WHERE {string.Join(" AND ", fields.Select(x => x + " = @" + x))}";

            using var connection = new SqlConnection(ConnectionString);
            return connection.Query(sql, param).FirstOrDefault();
        }

        public static IEnumerable<dynamic> GetAll(string table, object param = null)
        {
            var fields = param.GetProperties();

            string sql = $"SELECT * FROM {table} WHERE 1 = 1 {(fields.Any() ? "AND " + string.Join(" AND ", fields.Select(x => x + " = @" + x)) : "")}";

            using var connection = new SqlConnection(ConnectionString);
            return connection.Query(sql, param);
        }

        public static int Count( string table ) {
            string sql = $"SELECT COUNT(*) AS count FROM {table}";

            using var connection = new SqlConnection( ConnectionString );
            return (int) connection.QueryFirst( sql ).count;
        }

        public static dynamic Last(string table)
        {
            var sql = $"SELECT TOP 1 * FROM {table} ORDER BY {table}_id DESC";
            using var connection = new SqlConnection(ConnectionString);
            return connection.QueryFirst(sql);
        }

        public static dynamic Find(string table, object param)
        {
            var value = Get(table, param);
            if (value == null)
                value = Insert(table, param);

            return value;
        }
    }

    public static class AnonymousExtensions
    {
        public static IEnumerable<string> GetProperties(this object dataitem)
        {
            if (dataitem != null)
            {
                var type = dataitem.GetType();
                if (type.BaseType != null && type.BaseType.Name.Contains("Array"))
                    type = ((object[])dataitem)[0].GetType();

                var properties = type.GetProperties();
                return properties.Select(x => x.Name).Distinct().ToList();
            }
            return Enumerable.Empty<string>();
        }
    }
}