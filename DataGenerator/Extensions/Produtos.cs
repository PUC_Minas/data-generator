﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace DataGenerator.Extensions
{
    public class Produtos
    {
        public long Codigo { get; set; }
        public decimal Preco { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public DateTime Data_Cadastro { get; set; }
        public long Codigo_Fornecedor { get; set; }
        public string Nome_Fornecedor { get; set; }

        public static IEnumerable<Produtos> CarregarProdutos()
        {
            var produtos = JsonConvert.DeserializeObject<IEnumerable<Produtos>>(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "Assets", "dados_produtos.json")));

            return produtos
                .Where(x => x.Preco > 1)
                .ToList();
        }
    }
}